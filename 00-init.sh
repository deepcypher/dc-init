# @Author: George Onoufriou <archer>
# @Date:   2021-05-28T15:33:54+01:00
# @Last modified by:   archer
# @Last modified time: 2021-07-18T20:44:30+01:00



# detect architecture
architecture=""
case $(uname -m) in
    i386)   architecture="386" ;;
    i686)   architecture="386" ;;
    x86_64) architecture="amd64" ;;
    arm)    dpkg --print-architecture | grep -q "arm64" && architecture="arm64" || architecture="arm" ;;
    aarch64)   architecture="arm64" ;;
esac
echo "detected arch: ${architecture}"

keyprovider=""
if [ ${architecture} == "arm64" ]; then
  echo "assigning arm64 specific variables"
  keyprovider="archlinuxarm"
elif [ ${architecture} == "x86_64" ]; then
  echo "assigning x86_64 specific variables"
  keyprovider="archlinux"
else
  echo "Cannot handle this architecture: ${architecture} yet"
  exit 1
fi

# install
rm -rf /etc/pacman.d/gnupg && \
    pacman-key --init && \
    pacman-key --populate ${keyprovider} \
    || exit 1

yes | pacman -Syyuu --noconfirm \
  archlinux-keyring \
  ca-certificates \
  base-devel \
  git \
  neovim \
  fish \
  curl \
  python \
  sudo \
  docker \
  htop \
  iotop \
  ranger \
  lm_sensors \
  rxvt-unicode \
  glusterfs \
  || exit 1

yes | pacman -S \
  conntrack-tools \
  ethtool \
  iptables-nft \
  rsync \
  socat \
  gnu-netcat \
  ansible \
  || exit 1

systemctl start docker || exit 1
systemctl enable docker || exit 1

echo "installing necessary user"
useradd -m archer
echo "archer ALL=(ALL) ALL" > /etc/sudoers.d/archer
echo "archer ALL=(ALL) NOPASSWD:/usr/bin/pacman" >> /etc/sudoers.d/archer || exit 1
echo "archer ALL=(ALL) NOPASSWD:/usr/bin/pikaur" >> /etc/sudoers.d/archer || exit 1
# mkdir -p /home/archer/.ssh
git clone https://gitlab.com/deepcypher/dc-node-ssh /home/archer/.ssh
cd /home/archer/.ssh || exit 1
git pull || exit 1
sudo -u archer mkdir -p /home/archer/git
sudo -u archer git clone https://aur.archlinux.org/pikaur /home/archer/git/pikaur
cd /home/archer/git/pikaur || exit 1
sudo -u archer makepkg -s -f --noconfirm || exit 1
pacman -U *pkg.tar* --noconfirm || exit 1

echo "Ok time to install kubernetes"
if [ ${architecture} == "x86_64" ]; then
  sudo -u archer pikaur -S cni-plugins kubelet kubeadm kubectl flux-go kustomize downgrade helm --noconfirm
else
  sudo -u archer pikaur -S cni-bin kubeadm-bin kubelet-bin kubectl-bin flux-go kustomize downgrade helm --noconfirm
fi

git clone https://gitlab.com/deepcypher/dc-init.git /root/dc-init
cd /root/dc-init
git pull
cp -r ./.config ${HOME}/.config

git clone https://gitlab.com/deepcypher/dc-cluster.git /root/dc-cluster
cd /root/dc-cluster
git pull

# # https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/
#
# CNI_VERSION="v0.8.2"
# sudo mkdir -p /opt/cni/bin
# curl -L "https://github.com/containernetworking/plugins/releases/download/${CNI_VERSION}/cni-plugins-linux-amd64-${CNI_VERSION}.tgz" | sudo tar -C /opt/cni/bin -xz
#
# DOWNLOAD_DIR=/usr/local/bin
# sudo mkdir -p $DOWNLOAD_DIR
#
# CRICTL_VERSION="v1.17.0"
# curl -L "https://github.com/kubernetes-sigs/cri-tools/releases/download/${CRICTL_VERSION}/crictl-${CRICTL_VERSION}-linux-amd64.tar.gz" | sudo tar -C $DOWNLOAD_DIR -xz
#
# RELEASE="$(curl -sSL https://dl.k8s.io/release/stable.txt)"
# cd $DOWNLOAD_DIR
# sudo curl -L --remote-name-all https://storage.googleapis.com/kubernetes-release/release/${RELEASE}/bin/linux/amd64/{kubeadm,kubelet,kubectl}
# sudo chmod +x {kubeadm,kubelet,kubectl}
#
# RELEASE_VERSION="v0.4.0"
# curl -sSL "https://raw.githubusercontent.com/kubernetes/release/${RELEASE_VERSION}/cmd/kubepkg/templates/latest/deb/kubelet/lib/systemd/system/kubelet.service" | sed "s:/usr/bin:${DOWNLOAD_DIR}:g" | sudo tee /etc/systemd/system/kubelet.service
# sudo mkdir -p /etc/systemd/system/kubelet.service.d
# curl -sSL "https://raw.githubusercontent.com/kubernetes/release/${RELEASE_VERSION}/cmd/kubepkg/templates/latest/deb/kubeadm/10-kubeadm.conf" | sed "s:/usr/bin:${DOWNLOAD_DIR}:g" | sudo tee /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
