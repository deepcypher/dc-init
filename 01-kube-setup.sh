#!/usr/bin/env bash

# @Author: GeorgeRaven <archer>
# @Date:   2021-01-04T23:16:39+00:00
# @Last modified by:   archer
# @Last modified time: 2021-01-06T11:37:08+00:00
# @License: please see LICENSE file in project root

# /etc/docker/daemon.json
# {
#   "exec-opts": ["native.cgroupdriver=systemd"],
#   "log-driver": "json-file",
#   "log-opts": {
#     "max-size": "100m"
#   },
#   "storage-driver": "overlay2",
#   "storage-opts": [
#     "overlay2.override_kernel_check=true"
#   ]
# }

# edit kubelet to use systemd rather than cgroup
sudo sed -i -E 's/(^KUBELET_ARGS=).*/\1--cni-bin-dir=\/usr\/lib\/cni,--cgroup-driver="systemd"\n/gm' /etc/kubernetes/kubelet.env
# sudo nvim /etc/kubernetes/kubelet.env

# kubeadm init the cluster with specified cidr
# remember that IANA reserves the following ranges:
# - 10.0.0.1 to 10.255.255.254
# - 172.16.0.1 to 172.31.255.254
# - 192.168.0.1 to 192.168.255.254
sudo kubeadm init --pod-network-cidr="10.85.0.0/16"

# edit now generated kubelet config to use systemd rather than cgroup
# evictionHard:
#   imagefs.available: 1%
#   memory.available: 100Mi
#   nodefs.available: 1%
#   nodefs.inodesFree: 1%
sudo sed -i -E 's/(^cgroupDriver:).*/\1 "systemd"/gm' /var/lib/kubelet/config.yaml
# sudo nvim /var/lib/kubelet/config.yaml

# move kube config
mkdir -p ~/.kube/
sudo cp -i /etc/kubernetes/admin.conf ~/.kube/config
sudo chown $(id -u):$(id -g) ~/.kube/config

# change taint on node to be schedulable
kubectl taint nodes --all node-role.kubernetes.io/master-

# get calico manifest
curl https://docs.projectcalico.org/manifests/calico.yaml -O
# edit calico with cidr used in kubeadm
# sed -i -E 's/(# )(- name: CALICO_IPV4POOL_CIDR\n.*)(# )(.*: ).*/\2\4"10.85.0.0\/16"/gm' calico.yaml
#  # this regex groups and ignores the comment # to uncomment the lines and replaces CIDR with one provided
# nvim calico.yaml
# apply calico plugin now that its edited
kubectl apply -f calico.yaml

# apply calicotl as container in kube
kubectl apply -f https://docs.projectcalico.org/manifests/calicoctl.yaml
