# @Author: George Onoufriou <archer>
# @Date:   2021-06-04T16:09:21+01:00
# @Last modified by:   archer
# @Last modified time: 2021-07-16T21:36:24+01:00


# SINGLE POD
# # get calico manifest
# curl https://docs.projectcalico.org/manifests/calico.yaml -O
# # apply calico plugin now that its edited
# kubectl apply -f calico.yaml
# # apply calicotl as container in kube
# kubectl apply -f https://docs.projectcalico.org/manifests/calicoctl.yaml


# TIGERA
curl https://docs.projectcalico.org/manifests/calico-typha.yaml -o calico.yaml
nvim calico.yaml || exit 1
kubectl apply -f calico.yaml

# WEAVE
# kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"
