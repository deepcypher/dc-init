# @Author: George Onoufriou <archer>
# @Date:   2021-06-03T00:37:24+01:00
# @Last modified by:   archer
# @Last modified time: 2021-07-18T15:10:04+01:00

kubeadm reset
rm -r /etc/cni/net.d/
rm -r ~/.kube/config

docker rm -f $(docker ps -qa)
docker volume rm $(docker volume ls -q)
cleanupdirs="/var/lib/etcd /etc/cni/net.d /var/lib/cni /var/run/calico /opt/rke" # /opt/cni /etc/kubernetes
for dir in $cleanupdirs; do
  echo "Removing $dir"
  rm -rf $dir
done

iptables -F && iptables -t nat -F && iptables -t mangle -F && iptables -X
systemctl daemon-reload
systemctl restart kubelet
