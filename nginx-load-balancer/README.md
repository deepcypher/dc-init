# SOFTWARE LOAD BALANCER

For high availability it may be necessary to configure a software load balancer. We use our own openwrt openwrt3200ACMs with an nginx reverse proxy to serve the kubeapi server and act as our loadbalancer.

Conveniently `openwrt.lan` resolves to the address of the local openwrt host, which we append with the port of the API server 6443.

You will find an example in this folder of the main nginx.conf with the stream directive to our api.
